//
//  ViewController.swift
//  NaRogah
//
//  Created by User on 21/02/2019.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var viewButton: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onButtonTap(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Scheme")
        navigationController?.pushViewController(vc!, animated: true)
    }
    
}

